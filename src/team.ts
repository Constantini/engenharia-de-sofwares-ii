import { Player } from './player';

type Result = 'vitoria' | 'empate' | 'derrota';

interface ITeam {
  addPlayer(player: Player): void;
  deletePlayer(player: Player): boolean;
  getPoints(): number;
  addPoins(result: Result): void;
  getPlayer(playerNumber: number): Player | null;
}

export class Team implements ITeam {
  constructor(private team: Player[], private points: number = 0) {}

  addPlayer(player: Player): void {
    this.team.push(player);
  }

  deletePlayer(player: Player): boolean {
    const index = this.team.findIndex((item) => item.number === player.number);

    if (index > -1) {
      this.team.splice(index, 1);
      return true;
    }

    return false;
  }

  getPoints(): number {
    return this.points;
  }

  addPoins(result: Result): void {
    if (result === 'vitoria') {
      this.points += 3;
    } else if (result === 'empate') {
      this.points += 1;
    }
  }

  getPlayer(playerNumber: number): Player | null {
    const index = this.team.findIndex((item) => item.number === playerNumber);

    return this.team[index] || null;
  }
}
