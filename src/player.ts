type Positions = 'goleiro' | 'atacante' | 'defensor';

interface IPlayer {
  addGol(): void;
  getGols(): number;
  getNumber(): number;
  getPosition(): Positions;
  getAge(): number;
}

export class Player implements IPlayer {
  constructor(
    readonly number: number,
    readonly position: Positions,
    private age,
    private gols: number = 0,
  ) {}

  addGol() {
    this.gols += 1;
  }

  getGols() {
    return this.gols;
  }

  getNumber() {
    return this.number;
  }

  getPosition() {
    return this.position;
  }

  getAge() {
    return this.age;
  }
}
