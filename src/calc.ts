export default {
  goleiro: (altura: number, reflexos: number) => altura * 4 + reflexos * 6,
  defensor: (cobertura: number, desarme: number) => cobertura * 6 + desarme * 4,
  atacante: (velocidade: number, tecnica: number) => velocidade * 4 + tecnica * 6,
};
