import { Team } from './team';

interface IMatch {
  getTeams(): [Team, Team];
  getScoreboard(): [number, number];
  addScoreboard(team: 0 | 1, playerNumber: number): boolean;
}

export class Match implements IMatch {
  private scoreboard: [number, number] = [0, 0];

  constructor(private readonly teams: [Team, Team]) {}

  getTeams(): [Team, Team] {
    return this.teams;
  }

  getScoreboard(): [number, number] {
    return this.scoreboard;
  }

  addScoreboard(team: 0 | 1, playerNumber: number): boolean {
    const player = this.teams[team].getPlayer(playerNumber);

    if (player) {
      this.scoreboard[team] += 1;
      player.addGol();
      return true;
    }

    return false;
  }
}
